Dropzone.autoDiscover = false;
$(window).on("load", function () {
    $(".modal-w4c").modal("show");
});
$(".dropzone").dropzone({
    url: "upload.php",
    margin: 20,
    allowedFileTypes: "image.*, pdf, doc",
    params: {
        action: "save",
    },
    uploadOnDrop: true,
    uploadOnPreview: false,
    autoProcessQueue: false,
    success: function (res, index) {
        console.log(res, index);
    },
});
$("#myToggle").on("change", function (e) {
    if ($(this).is(":checked")) {
        $("input:radio[name=warning]").prop("disabled", false);
        $(".icheck-warning").removeClass("text-muted");
    } else {
        $("input:radio[name=warning]").prop("disabled", true);
        $("input:radio[name=warning]").prop("checked", false);
        $(".icheck-warning").addClass("text-muted");
    }
});